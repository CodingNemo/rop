import inspect
from collections import namedtuple
from functools import partial
from typing import Union, Any, Callable
from uuid import uuid4

MyDTO = namedtuple("MyDTO", ["val"])

Success = namedtuple("Success", ["val"])
Failure = namedtuple("Failure", ["err"])
Result = Union[Success, Failure]


class Pipe:
    class LeftToRight:
        def __init__(self, left):
            self.left = left

        def __gt__(self, right):
            signature = inspect.signature(right)
            if len(signature.parameters) == 1:
                return right(self.left)
            return partial(right, self.left)

    def __ror__(self, left):
        return Pipe.LeftToRight(left)


def _map(f, a) -> Result:
    if isinstance(a, Success):
        return Success(f(a.val))
    if isinstance(a, Failure):
        return Failure(a.err)


class MapPipe:
    class LeftToRight:

        def __init__(self, x):
            self.x = x

        def __gt__(self, other):
            signature = inspect.signature(other)
            if len(signature.parameters) == 1:
                return _map(other, self.x)
            return partial(_map, partial(other, self.x))

    def __ror__(self, other):
        return MapPipe.LeftToRight(other)


def bind(f, a) -> Result:
    if isinstance(a, Success):
        return f(a.val)
    if isinstance(a, Failure):
        return Failure(a.err)
    raise Exception(f"UNKNWOW RESULT {a}")


class BindPipe:
    class LeftToRight:
        def __init__(self, x):
            self.x = x

        def __gt__(self, other):
            signature = inspect.signature(other)
            if len(signature.parameters) == 1:
                return bind(other, self.x)
            return partial(bind, partial(other, self.x))

    def __ror__(self, other):
        return BindPipe.LeftToRight(other)


def compose(add, f1, f2):
    def call(a):
        val1 = f1(a)
        val2 = f2(a)

        if isinstance(val1, Success) and isinstance(val2, Success):
            return Success(a)

        if isinstance(val1, Failure) and isinstance(val2, Failure):
            return Failure(add(val1.err, val2.err))

        if isinstance(val1, Failure):
            return Failure(val1.err)

        if isinstance(val2, Failure):
            return Failure(val2.err)

    return call


class Infix:
    """ TRICK CLASS TO CREATE CUSTOM OPERATOR"""

    def __init__(self, operator_function: Callable[[Any, Any], Any]):
        self.operator_function = operator_function

    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.operator_function(other, x))

    def __or__(self, other):
        return self.operator_function(other)

    def __rlshift__(self, other):
        return Infix(lambda x, self=self, other=other: self.operator_function(other, x))

    def __rshift__(self, other):
        return self.operator_function(other)

    def __call__(self, val):
        return self.operator_function(val)


_ = Pipe()
b = BindPipe()
m = MapPipe()
o = Infix(partial(compose, lambda r, l: r + l))


def convert(a) -> MyDTO:
    return MyDTO(a)


def do_save(a):
    if not isinstance(a, MyDTO):
        raise Exception(f"Could not save something that is not an instance of '{MyDTO}'")
    return uuid4(), a


def handle_error(act_on, val) -> Result:
    try:
        return Success(act_on(val))
    except Exception as e:
        return Failure([str(e)])


def ensure_positive(val) -> Result:
    if val < 0:
        return Failure(["Value should be positive"])
    return Success(val)


def ensure_even(val) -> Result:
    if val % 2 != 0:
        return Failure(["Value should be even"])
    return Success(val)


def ensures_stricly_less_than_10(val) -> Result:
    if val >= 10:
        return Failure(["Value should be strictly less than 10"])
    return Success(val)


def save(val) -> Result:
    return ((val
             |_> (ensure_even | o | ensure_positive | o | ensures_stricly_less_than_10))
             |m> convert) \
             |b> (do_save |_> handle_error)
